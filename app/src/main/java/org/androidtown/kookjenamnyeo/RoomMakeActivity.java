package org.androidtown.kookjenamnyeo;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.icu.util.Calendar;
import android.icu.util.GregorianCalendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import java.util.ArrayList;
import java.util.List;

import static org.androidtown.kookjenamnyeo.R.id.Create_btn;

public class RoomMakeActivity extends AppCompatActivity {

    private static final String TAG = "RoomMake";

    private FirebaseDatabase mDatabase;
    GregorianCalendar calendar;

    private HashTagHelper mTextHashTagHelper;
    private TextView mHashTagText;

    public String test;

    long startTime;

    int year, month, day, hour, minute;
    int real_year, real_month, real_day, real_hour, real_minute;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_make);
        FirebaseApp.initializeApp(this);

        //FireBase
        mDatabase = FirebaseDatabase.getInstance();

        // Hash Tag
        mHashTagText = (TextView) findViewById(R.id.editTag);
        mTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.colorPrimary),
                new HashTagHelper.OnHashTagClickListener() {
                    @Override
                    public void onHashTagClicked(String hashTag) {
                        Log.d(TAG, hashTag);
                    }
                });

        // pass a TextView or any descendant of it (incliding EditText) here.
        // Hash tags that are in the text will be hightlighed with a color passed to HasTagHelper
        mTextHashTagHelper.handle(mHashTagText);

        test = mHashTagText.getText().toString();
        Log.d(TAG, test);

        //Picker

        calendar = (GregorianCalendar) new GregorianCalendar().getInstance();

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        findViewById(R.id.Date_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(RoomMakeActivity.this, dateSetListener, year, month, day).show();

            }
        });

        findViewById(R.id.Time_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(RoomMakeActivity.this, timeSetListener, hour, minute, false).show();
            }
        });


        final EditText editTitle = (EditText) findViewById(R.id.editText);
        final Spinner spin_loc = (Spinner) findViewById(R.id.spinner_location);
        final Spinner spin_max = (Spinner) findViewById(R.id.spinner_max);
        final EditText editTag = (EditText) findViewById(R.id.editTag);


        findViewById(Create_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = editTitle.getText().toString();
                String hashtags = editTag.getText().toString();
//                String hostId;
                calendar.set(real_year, real_month, real_day,real_hour,real_minute, 0);
                Long date = calendar.getTimeInMillis();

//                String max = spin_max.getSelectedItem().toString();

                List<String> particiPants = new ArrayList<>();
                particiPants.add("user1");
                particiPants.add("user2");

                Parties parties = new Parties(title, hashtags , "hostID", date, 4, particiPants);

                String pid = mDatabase.getReference().push().getKey();

                mDatabase.getReference().child("parties").child(pid).setValue(parties.toMap());

                Toast.makeText(RoomMakeActivity.this, "성공적으로 방만들기 완료", Toast.LENGTH_SHORT).show();
                finish();


            }
        });



    }

    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub

            real_year = year;
            real_month = monthOfYear;
            real_day = dayOfMonth;

            String msg = String.format("%d-%d-%d", year,monthOfYear+1, dayOfMonth);
            Toast.makeText(RoomMakeActivity.this, msg, Toast.LENGTH_SHORT).show();

            TextView DTxt = (TextView) findViewById(R.id.Date_text);
            DTxt.setText(msg);

        }

    };

    TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // TODO Auto-generated method stub

            real_hour = hourOfDay;
            real_minute = minute;
            String msg = String.format("%d/%d:%d", year, hourOfDay, minute);
            Toast.makeText(RoomMakeActivity.this, msg, Toast.LENGTH_SHORT).show();

            TextView TTxt = (TextView) findViewById(R.id.Time_text);
            TTxt.setText(msg);

        }

    };



    public void goBack() {
        onBackPressed();
    }

//    public void onButtonCreate(String userId, String name, String email) {
//        Users user = new Users(name, email);
//        mDatabase.child("users").child(userId).setValue(user);
//
//        Log.d(TAG, test);
//    }

}
