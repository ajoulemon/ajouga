package org.androidtown.kookjenamnyeo;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.io.File;

import static android.R.attr.data;
import static com.google.firebase.FirebaseApp.initializeApp;

public class PhotoActivity extends AppCompatActivity {

    private static final int PICK_FROM_CAMERA = 0;
    private static final int PICK_FROM_ALBUM = 1;
    private static final int CROP_FROM_iMAGE = 2;

    private Uri mImageCaptureUri;
    private ImageView iv_UserPhoto;
    private int id_view;
    private String absoultePath;
    private StorageReference mStorageRef;
    private StorageTask uploadTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        FirebaseApp.initializeApp(this);
        iv_UserPhoto = (ImageView) findViewById(R.id.photo);

        mStorageRef = FirebaseStorage.getInstance().getReferenceFromUrl("gs://kookje-namnyeo-android.appspot.com");
    }

    public void onButton1Click(View v) {
        id_view = v.getId();
        if (v.getId() == R.id.btn_UploadPicture) {
//            DialogInterface.OnClickListener cameraListener = new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    doTakePhotoAction();
//                }
//            };
//
//            DialogInterface.OnClickListener albumListener = new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    doTakeAlbumAction();
//                }
//            };
//
//
//            DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            };
//
//            new AlertDialog.Builder(this)
//                    .setTitle("업로드할 이미지 선택")
//                    .setPositiveButton("사진촬영", cameraListener)
//                    .setNeutralButton("앨범선택", albumListener)
//                    .setNegativeButton("취소", cancelListener).show();
//

        }
    }


    public void doTakePhotoAction() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));


//        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, PICK_FROM_CAMERA);
    }

    public void doTakeAlbumAction() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        intent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
            case PICK_FROM_ALBUM: {
                mImageCaptureUri = data.getData();
                Log.d("SmartWheel", mImageCaptureUri.getPath().toString());
            }


            case PICK_FROM_CAMERA:
                iv_UserPhoto.setImageResource(0);
                iv_UserPhoto.setImageURI(data.getData());

                if (resultCode != RESULT_OK) {
                    return;
                }

                final Bundle extras = data.getExtras();

                String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                        "/ajouga/Profile/Images/" + System.currentTimeMillis() + ".jpg";

                if (mImageCaptureUri != null) {

                    extras.putParcelable(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                }

                if (extras != null) {
                    Bitmap photo = extras.getParcelable("data");
                    iv_UserPhoto.setImageBitmap(photo);


                    absoultePath = filePath;
                    break;
                }

                File f = new File(mImageCaptureUri.getPath());
                if (f.exists()) {
                    f.delete();
                }
                break;
        }
        uploadFile();
    }


        private void uploadFile(){
        if(mImageCaptureUri != null){

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("upload");
            progressDialog.show();

            StorageReference riversRef = mStorageRef.child("images/profile.jpg");

            uploadTask = riversRef.putFile(mImageCaptureUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "File Upload", Toast.LENGTH_LONG).show();

                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();

                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage(((int) progress) + "%upload...");

                        }
                    });

        } else {

        }

    }

//    public class MyApplication extends Application {
//        @Override
//        public void onCreate() {
//            super.onCreate();
//            SomeSdk.init(this);  // init some SDK, MyApplication is the Context
//        }
//    }

    }
//    private void storeCropImage(Bitmap bitmap, String filePath){
//        String dirPath =
//                Environment.getExternalStorageDirectory().getAbsolutePath()+"/SmartWheel";
//        File directory_SmartWheel = new File(dirPath);
//
//        if (!directory_SmartWheel.exists())
//            directory_SmartWheel.mkdir();
//
//        File copyFile = new File(filePath);
//        BufferedOutputStream out = null;
//
//        try{
//
//            copyFile.createNewFile();
//            out = new BufferedOutputStream(new FileOutputStream(copyFile));
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
//
//            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(copyFile)));
//
//            out.flush();
//            out.close();
//        } catch (Exception e){
//            e.printStackTrace();
//        }
//
//    }



