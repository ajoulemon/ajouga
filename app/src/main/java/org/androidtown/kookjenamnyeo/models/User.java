package org.androidtown.kookjenamnyeo.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by KOHANSOL on 2017-05-13.
 */
@IgnoreExtraProperties
public class User implements Serializable{
    private String uid;
    private String name;
    private String address;

    public User() {
    }

    public User(String uid, String name, String address) {
        this.uid = uid;
        this.name = name;
        this.address = address;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();    //map 형태로 들어감
        result.put("uid",uid);
        result.put("name",name);
        result.put("address",address);
        return result;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() { return name; }
























}
