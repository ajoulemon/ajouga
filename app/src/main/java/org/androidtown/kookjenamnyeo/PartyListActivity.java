package org.androidtown.kookjenamnyeo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.androidtown.kookjenamnyeo.models.Member;
import org.androidtown.kookjenamnyeo.models.Party;

import java.util.ArrayList;
import java.util.List;

public class PartyListActivity extends AppCompatActivity {
    private MyPagerAdapter adapter;
    private boolean btnChange = true;
    private Button add;
    private Button myHome;
    private ViewPager vp;
    private List<FirstFragment> firstFragments;

    List<Party> testParties;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_party_list);

//        FirstFragment firstFragment = new FirstFragment();
//        Bundle bundle = new Bundle();
//        bundle.putSerializable(party);
//        firstFragment.setArguments(bundle);

        add = (Button) findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   Intent intent = new Intent(getApplicationContext()
                //        , .class);
                //   startActivity(intent);
            }
        });

                myHome = (Button) findViewById(R.id.myHome);
        myHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnChange = !btnChange;
                if (btnChange) {
                    myHome.setBackgroundResource(R.drawable.ic_home_white);

                    Intent intent = new Intent(getApplicationContext()
                            , PartyListActivity.class);
                       startActivity(intent);

                } else {
                    myHome.setBackgroundResource(R.drawable.ic_home_pink);

                    Snackbar.make(v,"나의 방에 추가된 방만 보여집니다.", 2500).
                            setAction("취소", new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v)
                                {
                                }
                            }).show();
                }

            }
        });


        Member member = new Member("kk","한솔", false);
        List<Party> ps = new ArrayList<>();
        ps.add(new Party("3", "3번째 방"));
        ps.add(new Party("5", "5번째 방"));
        member.setJoinedParties(ps);

        testParties = new ArrayList<>();
        List<Member> ms = new ArrayList<>();
        ms.add(member);
        for (int i=0; i<10; i++) {
            Party p = new Party(""+i, ""+i+"번째 방");
            if (i == 3 || i == 5) {
                p.setJoinedMembers(ms);
            }
            testParties.add(p);
        }

        firstFragments = new ArrayList<>();

        for (int i=0; i<testParties.size(); i++) {
            FirstFragment fragment = new FirstFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("position", i);
            fragment.setArguments(bundle);
            firstFragments.add(fragment);
        }

        vp = (ViewPager)findViewById(R.id.vp);
        vp.setPageMargin(30);
        adapter = new MyPagerAdapter(getSupportFragmentManager());
        vp.setAdapter(adapter);
        adapter.setData(firstFragments);

    }

    View.OnClickListener movePageListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            int tag = (int) v.getTag();
            vp.setCurrentItem(tag);
        }
    };



    private class MyPagerAdapter extends FragmentStatePagerAdapter
    {
        private List<FirstFragment> fragments;
        public MyPagerAdapter(android.support.v4.app.FragmentManager fm)
        {
            super(fm);
        }
        @Override
        public FirstFragment getItem(int position)
        {
            if (null != fragments) {
                return fragments.get(position);
            } else {
                return null;
            }
        }
        @Override
        public int getCount()
        {
            if (null != fragments) {
                return fragments.size();
            } else {
                return 0;
            }
        }

        @Override
        public float getPageWidth(int position) {
            return super.getPageWidth(position)*0.8f;
        }

        public void setData(List<FirstFragment> data) {
            if (null == fragments) {
                fragments = new ArrayList<>();
                fragments = data;
            } else {
                fragments = data;
            }
            notifyDataSetChanged();
        }
    }
}
