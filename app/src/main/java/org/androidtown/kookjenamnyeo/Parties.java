package org.androidtown.kookjenamnyeo;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Workspace on 2017-05-14.
 */

public class Parties {

    // private String pid;
    private String title;
    private String hashtags;
    private String hostId;
    private Long date;
    private int max;
    private List<String> particiPants;

    public Parties() {
    }

    public Parties(String title, String hashtags, String hostId, Long date, int max, List<String> particiPants) {
        this.title = title;
        this.hashtags = hashtags;
        this.hostId = hostId;
        this.date = date;
        this.max = max;
        this.particiPants = particiPants;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap();
        result.put("title", title);
        result.put("hashtags", hashtags);
        result.put("hostId", hostId);
        result.put("date", date);
        result.put("max", max);
        result.put("particiPants", particiPants);
        return result;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHashtags() {
        return hashtags;
    }

    public void setHashtags(String hashtags) {
        this.hashtags = hashtags;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public List<String> getParticiPants() {
        return particiPants;
    }

    public void setParticiPants(List<String> particiPants) {
        this.particiPants = particiPants;
    }
}
