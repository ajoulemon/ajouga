package org.androidtown.kookjenamnyeo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import static com.facebook.FacebookSdk.getApplicationContext;

public class FirstFragment extends Fragment
{
    public FirstFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        Log.e("pagerFragment", ""+getArguments().getInt("position"));
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.fragment_first, container, false);
        return layout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    //    TextView textView = (TextView) view.findViewById(R.id.testText);
    //    textView.setText(getArguments().getInt("position")+"번째 페이지");

        RelativeLayout viewpagerLayout = (RelativeLayout) view.findViewById(R.id.viewpagerLayout);

        viewpagerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext()
                         , SplashActivity.class);
                startActivity(intent);
            }
        });
    }
}
